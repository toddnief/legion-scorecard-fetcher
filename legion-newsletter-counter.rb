require "uri"
require "net/http"
require "pry"
require 'json'
require 'base64'
require 'dotenv'

# load environment variables
Dotenv.load

# infusionsoft app details
KEAP_CLIENT_ID = "K0SjU6Qz1CPJjkRtPucnpTef4D6dHFVW"
KEAP_CLIENT_SECRET = ENV["KEAP_CLIENT_SECRET"]
refresh_token = File.open("refresh_token.txt").read # read the previous access token from file

# binding.pry

refresh_auth = "Basic " + Base64.strict_encode64(KEAP_CLIENT_ID + ':' + KEAP_CLIENT_SECRET) # encode client ID and client secret for auth

# refresh bearer token
url = URI("https://api.infusionsoft.com/token")

https = Net::HTTP.new(url.host, url.port);
https.use_ssl = true

request = Net::HTTP::Post.new(url)
request["Authorization"] = refresh_auth
request["Content-Type"] = "application/x-www-form-urlencoded"
request.body = "grant_type=refresh_token&refresh_token=#{refresh_token}"

response = https.request(request)
response = JSON.parse(response.body)

access_token = response["access_token"]
new_refresh_token = response["refresh_token"]

File.write("refresh_token.txt", new_refresh_token)

# binding.pry

# define variables for request
legion_newsletter_tag_id = 562

# submit infusionsoft request
url = URI("https://api.infusionsoft.com/crm/rest/v1/tags/#{legion_newsletter_tag_id}/contacts?access_token=#{access_token}")

https = Net::HTTP.new(url.host, url.port);
https.use_ssl = true

request = Net::HTTP::Get.new(url)

response = https.request(request)
response = JSON.parse(response.body)

# extract the count from the response—this is just included in the response as a variable
legion_newsletter_count = response["count"]

# binding.pry
# puts legion_newsletter_count

# send newsletter count to Zapier to update Google Sheets
url = URI("https://hooks.zapier.com/hooks/catch/1093047/olnxw2r/?legion_newsletter_count=#{legion_newsletter_count}")

https = Net::HTTP.new(url.host, url.port);
https.use_ssl = true

request = Net::HTTP::Post.new(url)

response = https.request(request)
